# Gaia-X Trust Framework Ontology (minimal version)

By [Roberto García](https://rhizomik.net/~roberto/). These are just my personal vision and interpretation of parts of
the Gaia-X specification using my experience with Semantic Web technologies.

## Documentation

The documentation, that can be commented and discussed, is available from: https://hackmd.io/@rogargon/H1S9q03l3

[![](https://i.imgur.com/08SaBab.png)](https://hackmd.io/@rogargon/H1S9q03l3)
